﻿using System;
using System.Collections.Generic;

namespace NekioriProyectCoreMVC.Models
{
    public partial class Cliente
    {
        public Cliente()
        {
            Casos = new HashSet<Casos>();
        }

        public int Id { get; set; }
        public string Empresa { get; set; }
        public string Direccion { get; set; }
        public string Ciudad { get; set; }
        public string Comuna { get; set; }
        public string Contacto { get; set; }
        public string Telefono1 { get; set; }
        public string Telefono2 { get; set; }
        public string Correo { get; set; }

        public virtual ICollection<Casos> Casos { get; set; }
    }
}
