﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace NekioriProyectCoreMVC.Models
{
    public partial class CUSERSNEKLABDOCUMENTSNEKIORIMDFContext : DbContext
    {
        public CUSERSNEKLABDOCUMENTSNEKIORIMDFContext()
        {
        }

        public CUSERSNEKLABDOCUMENTSNEKIORIMDFContext(DbContextOptions<CUSERSNEKLABDOCUMENTSNEKIORIMDFContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Casos> Casos { get; set; }
        public virtual DbSet<Cliente> Cliente { get; set; }
        public virtual DbSet<Disco> Disco { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Server=(localdb)\\mssqllocaldb;Database=C:\\USERS\\NEKLAB\\DOCUMENTS\\NEKIORI.MDF;Trusted_Connection=True;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.6-servicing-10079");

            modelBuilder.Entity<Casos>(entity =>
            {
                entity.HasKey(e => e.IdCasos)
                    .HasName("PK__tmp_ms_x__400E003E92C5F338");

                entity.HasIndex(e => e.NroCaso)
                    .HasName("UQ__tmp_ms_x__08E0D133A1F6C2A3")
                    .IsUnique();

                entity.Property(e => e.IdCasos)
                    .HasColumnName("Id_Casos")
                    .ValueGeneratedNever();

                entity.Property(e => e.CantDatos)
                    .HasColumnName("Cant_datos")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Estado)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.FechaIngreso)
                    .HasColumnName("fecha_ingreso")
                    .HasColumnType("date");

                entity.Property(e => e.IdCliente).HasColumnName("Id_Cliente");

                entity.Property(e => e.Listado)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.NroCaso).HasColumnName("Nro_Caso");

                entity.HasOne(d => d.IdClienteNavigation)
                    .WithMany(p => p.Casos)
                    .HasForeignKey(d => d.IdCliente)
                    .HasConstraintName("fk_casos_cliente");
            });

            modelBuilder.Entity<Cliente>(entity =>
            {
                entity.HasIndex(e => e.Correo)
                    .HasName("UQ__Cliente__60695A19FAA04441")
                    .IsUnique();

                entity.HasIndex(e => e.Telefono1)
                    .HasName("UQ__Cliente__5AB0D5D139C11753")
                    .IsUnique();

                entity.Property(e => e.Ciudad)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Comuna)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Contacto)
                    .IsRequired()
                    .HasMaxLength(70)
                    .IsUnicode(false);

                entity.Property(e => e.Correo)
                    .IsRequired()
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.Direccion)
                    .IsRequired()
                    .HasMaxLength(60)
                    .IsUnicode(false);

                entity.Property(e => e.Empresa)
                    .HasMaxLength(120)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('Particular')");

                entity.Property(e => e.Telefono1)
                    .IsRequired()
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.Property(e => e.Telefono2)
                    .HasMaxLength(12)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Disco>(entity =>
            {
                entity.HasKey(e => e.IdDisco)
                    .HasName("PK__Disco__AD209AC0E6DB4658");

                entity.HasIndex(e => e.Serial)
                    .HasName("UQ__Disco__1A00E093BF4CD7AC")
                    .IsUnique();

                entity.Property(e => e.IdDisco)
                    .HasColumnName("Id_Disco")
                    .ValueGeneratedNever();

                entity.Property(e => e.Capacidad)
                    .IsRequired()
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.IdCaso).HasColumnName("Id_caso");

                entity.Property(e => e.Interfaz)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Marca)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Modelo)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Serial)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TipoDispositivo)
                    .IsRequired()
                    .HasColumnName("Tipo_Dispositivo")
                    .HasMaxLength(25)
                    .IsUnicode(false);

                entity.HasOne(d => d.IdCasoNavigation)
                    .WithMany(p => p.Disco)
                    .HasForeignKey(d => d.IdCaso)
                    .HasConstraintName("fk_disco_caso");
            });
        }
    }
}
